#!/usr/bin/env python
"""This is a REST api towards the Arch Linux package manager.
"""
from flask import Flask, jsonify

import pacman.lib

app = Flask(__name__)


@app.route('/')
@app.route('/upgrades')
def upgrades():
    try:
        upgrades = list(pacman.lib.yield_upgrades())
    except pacman.lib.PacmanException as exc:
        return str(exc)
    else:
        return jsonify(upgrades)


@app.route('/orphans')
def orphans():
    try:
        orphans = pacman.lib.orphans()
    except pacman.lib.PacmanException as exc:
        return str(exc)
    else:
        return jsonify(orphans)


@app.route('/orphans/remove')
def remove_orphans():
    removed = pacman.lib.remove_orphans()
    if removed:
        return jsonify(removed)
    else:
        return "No orphans found"



@app.route('/versions')
def versions():
    return jsonify(pacman.lib.current_versions())
