#!/usr/bin/env python
import subprocess


def run(args, check=True):
    """Run COMMAND and return lines"""
    completed_process = subprocess.run(
        args, check=check, universal_newlines=True, stdout=subprocess.PIPE
    )
    return completed_process.stdout.splitlines()