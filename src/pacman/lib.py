#!/usr/bin/env python
"""Python API for the Arch Linux package manager.
"""
from utils import run


class PacmanException(Exception):
    pass


def upgrade():
    """Upgrade system"""
    command = "pacman -Syu"
    run(command.split())


def update():
    """Update package versions"""
    command = "pacman -Sy"
    run(command.split())


def install(package):
    """Install PACKAGE on system"""
    command = f"pacman -S {package}"
    run(command.split())


def current_packages_info():
    """Return a dictionary with all packages current versions"""
    lines = run("pacman -Si".split())
    def get_text_chunks():
        chunk = []
        for line in lines:
            if line.strip():
                chunk.append(line)
            else:
                yield chunk
                chunk = []

    package_info_texts = get_text_chunks()
    for info_text in package_info_texts:
        yield _package_info_text_to_dict(info_text)


def current_versions():
    return {p["Name"]: p["Version"] for p in current_packages_info()}


def yield_upgrades():
    update()
    lines = run("pacman -Qu".split(), check=False)
    if not lines:
        raise PacmanException("No pending upgrades")
    #  example line: xfce4-clipman-plugin 1.4.3-2 -> 1.4.4-1
    for line in lines:
        package, current_version, _, next_version = line.strip().split(" ")
        yield {
            package: {
                "current": current_version,
                "next": next_version
            } 
        }


def orphans():
    lines = run("pacman -Qdt".split(), check=False)
    if not lines:
        raise PacmanException("No orphans found")
    package_and_version = (l.split() for l in lines)
    return {p: v for p, v in package_and_version}


def remove_orphans():
    orphan_packages = orphans()
    if 0 < len(orphan_packages):
        names = list(orphan_packages.keys())
        command = ["pacman", "-Rns"] + names
        run(command)
        return orphan_packages


def _package_info_text_to_dict(info_text):
    package_info_dict = {}
    for line in info_text:
        try:
            field_raw, value_raw = line.split(":", maxsplit=1)
        except ValueError:
            pass  # TODO: maybe log this
        else:
            package_info_dict[field_raw.strip()] = value_raw.strip()
    return package_info_dict
