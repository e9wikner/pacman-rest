"""Tests for lib.py"""
import pytest

from pacman import lib


@pytest.fixture
def package_info_text():
    return """Repository      : core
Name            : acl
Version         : 2.2.53-2
Description     : Access control list utilities, libraries and headers
Architecture    : x86_64
URL             : https://savannah.nongnu.org/projects/acl
Licenses        : LGPL
Groups          : None
Provides        : xfsacl
Depends On      : attr
Optional Deps   : None
Conflicts With  : xfsacl
Replaces        : xfsacl
Download Size   : 131,94 KiB
Installed Size  : 323,45 KiB
Packager        : Allan McRae <allan@archlinux.org>
Build Date      : tis 12 nov 2019 09:56:53
Validated By    : MD5 Sum  SHA-256 Sum  Signature""".splitlines()


@pytest.fixture
def expected_package_info_dict():
    return {
        "Repository": "core",
        "Name": "acl",
        "Version": "2.2.53-2",
        "Description": "Access control list utilities, libraries and headers",
        "Architecture": "x86_64",
        "URL": "https://savannah.nongnu.org/projects/acl",
        "Licenses": "LGPL",
        "Groups": "None",
        "Provides": "xfsacl",
        "Depends On": "attr",
        "Optional Deps": "None",
        "Conflicts With": "xfsacl",
        "Replaces": "xfsacl",
        "Download Size": "131,94 KiB",
        "Installed Size": "323,45 KiB",
        "Packager": "Allan McRae <allan@archlinux.org>",
        "Build Date": "tis 12 nov 2019 09:56:53",
        "Validated By": "MD5 Sum  SHA-256 Sum  Signature",
    }


def test_package_text_to_dict(package_info_text, expected_package_info_dict):
    package_info_dict = lib._package_info_text_to_dict(package_info_text)
    assert expected_package_info_dict == package_info_dict


@pytest.fixture
def current_packages_info():
    return lib.current_packages_info()


def test_current_packages_info(current_packages_info):
    for info in current_packages_info:
        assert "Name" in info


@pytest.fixture
def current_versions():
    return lib.current_versions()


def test_current_versions(current_versions):
    assert "pacman" in current_versions
