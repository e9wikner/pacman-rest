FROM archlinux:latest AS arch
RUN pacman -Syu --noconfirm

FROM arch AS flask
RUN pacman -S --noconfirm --needed --asdeps python-flask

FROM flask AS app
COPY src/ /app/

FROM app as tester
RUN pacman -S --noconfirm --needed --asdeps python-pytest

FROM tester
COPY tests /app/tests
WORKDIR /app
RUN ls -la * && pytest -v tests

FROM app
WORKDIR /app
EXPOSE 80
ENV FLASK_APP app.py
ENV FLASK_DEBUG 1
CMD ["flask", "run", "--port", "80", "--host", "0.0.0.0"]