from pathlib import Path
import sys

srcdir = Path(__file__).parent.parent
sys.path.insert(0, str(srcdir.resolve()))